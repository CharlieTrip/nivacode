# Non-Interactive, Secure Verifiable Aggregation for Decentralized, Privacy-Preserving Learning #

*Published Paper:* [ACISP 2021](https://data61dsslab.github.io/acisp2021/)
*Full Paper:* [ePrint](https://eprint.iacr.org/2021/654)

### Content of the Repo ###
* `main.py`: main code to execute the experiment and contains the experiment parameters


### Quick Usage ###
* Modify the experimental parameters in `main.sh`
* Run `main.sh` 
